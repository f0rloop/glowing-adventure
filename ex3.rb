puts "I will now count my chickens:"
#Divides 30 with 6 then adds 25 + 5.
puts "Hens", 25 + 30 / 6
#3 % 4 = 0.1, then multiply with 25 = 2.5. 100 - 2.5 = 97.5
puts "Roosters", 100 - 25 * 3 % 4

puts "Now I will count the eggs:"
#MDAS principle
puts 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6
#Nothing but string
puts "Is it true that 3 + 2 < 5 - 7?"
#returns false
puts 3 + 2 < 5 - 7
#returns number 5
puts "What is 3 + 2?", 3 + 2
#returns -2
puts "What is 5 - 7?", 5 - 7
#nothing but string
puts "Oh, that's why it's false."
#nothing but string
puts "How about some more."
#returns true
puts "Is it greater?", 5 > -2
#returns true
puts "Is it greater or equal?", 5 >= -2
#returns false
puts "Is it less or equal?", 5 <= -2